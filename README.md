# Programacon Concurrente UNLP Recursada 2017
<br>
En este repositorio se encuentran ejercicios prácticos resueltos de la materia **Programacon Concurrente**.
<br><br>
## Prácticas 
* [Práctica 1](https://gitlab.com/sebaesg/concurrente-unlp/blob/master/Practica1/Practica1.md) :white_check_mark:
* [Práctica 2](https://gitlab.com/sebaesg/concurrente-unlp/blob/master/Practica2/Practica.md) :white_check_mark: