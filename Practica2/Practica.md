**PRACTICA 2**
<br><br>
**1)** Existen N personas que deben ser chequeadas por un detector de metales antes de poder
ingresar al avión.
<br>
<ul>
<li>Implemente una solución que modele el acceso de las personas a un detector (es decir
si el detector está libre la persona lo puede utilizar caso contrario debe esperar).</li>
<li>Modifique su solución para el caso que haya tres detectores.</li>
</ul>
```
sem semDetM=1
procces 1..n detector(){
	aduana();
	p(semDetM);
	DetectorDeMetales;
	v(semDetM);
	subirAlAvion;
}
```
<br>
```
**sem semDetM=3**
procces 1..n detector(){
	aduana();
	p(semDetM);
	DetectorDeMetales;
	v(semDetM);
	subirAlAvion;
}
```
<br><br>
**2)** Un sistema operativo mantiene 5 instancias de un recurso almacenadas en una cola,cuando un proceso necesita usar una instancia del recurso la saca de la cola, la usa y cuando termina de usarla la vuelve a depositar. 
<br>
```
sem semColaInstancias=1
instancias[5] instancias;
sem[5] semInstancia;
int prox=0;
procces 1..n proConsumidor(){
	...;
	p(colaInstancias);
		p(semInstancia[prox]);
		mySem=prox;
		prox=prox+1;
		prox=prox MOD 5;
	v(colaInstancias);
	usaInstancia();
	v(semInstancia[mySem]);
	...;
}
```
<br><br>
**3)** Suponga que existe una BD que puede ser accedida por 6 usuarios como máximo al mismo
tiempo. Además los usuarios se clasifican como usuarios de prioridad alta y usuarios de
prioridad baja. Por último la BD tiene la siguiente restricción:
• no puede haber más de 4 usuarios con prioridad alta al mismo tiempo usando la BD.
• no puede haber más de 5 usuarios con prioridad baja al mismo tiempo usando la BD.
Indique si la solución presentada es la más adecuada. Justifique la respuesta. 

|Var alta: semaphoro := 4;|sem: semaphoro := 6;baja: semaphoro := 5; |
|--|--|
|Process Usuario-Alta [I:1..L]::|Process Usuario-Baja [I:1..K]::|
| { P (sem);| { P (sem);|
| P (alta);| P (baja);|
| //usa la BD|//usa la BD|
| V(sem);|V(sem);|
| V(alta);| V(baja);|
| }|}|
```
* no es la mas adecuada dado que si entraran por ejemplo 6 usuario de alta prioridad y luego 2 usuarios de baja prioridad la BD estaria funcionando con los 4 usuarios alta sin dejar permitir entrar a los 2 usuarios baja .
```
<br><br>
**4)**Se tiene un curso con 40 alumnos, la maestra entrega una tarea distinta a cada alumno,
luego cada alumno realiza su tarea y se la entrega a la maestra para que la corrija, esta
revisa la tarea y si está bien le avisa al alumno que puede irse, si la tarea está mal le indica
los errores, el alumno corregirá esos errores y volverá a entregarle la tarea a la maestra
para que realice la corrección nuevamente, esto se repite hasta que la tarea no tenga
errores.

```
sem profesora=1;
procces [1..40] alumno(){
	recibirTarea()
	realizarTarea()
	P(profesora)
	while (!revisarTarea){
		V(profesora)
		realizarTarea()
		P(profesora)
	}
	V(profesora)
}
```
<br><br>
**5)** Suponga que se tiene un curso con 50 alumnos. Cada alumno elije una de las 10 tareas
para realizar entre todos. Una vez que todos los alumnos eligieron su tarea comienzan a
realizarla. Cada vez que un alumno termina su tarea le avisa al profesor y si todos los
alumnos que tenían la misma tarea terminaron el profesor les otorga un puntaje que
representa el orden en que se terminó esa tarea.
Nota: Para elegir la tarea suponga que existe una función elegir que le asigna una tarea a
un alumno (esta función asignará 10 tareas diferentes entre 50 alumnos, es decir, que 5
alumnos tendrán la tarea 1, otros 5 la tarea 2 y así sucesivamente para las 10 tareas).
```
sem [50] seguir=0;
contador=0;
sem elegir=1;
sem cont=1;
int gruposTermino[10]=0;
int gruposNota[10]=0;
int orden=1;
process[1..50] alumno(){
	P(elegir);
	miTarea=elegir();
	V(elegir);
	P(cont);
	if (contador==49){
		V(cont);
		for(i=0;i<49;i++){
			V(seguir[i])
		}
	}else{
		miNro=contador
		contador++;
		V(cont)
		P(seguir[miNro]);
	}
	hacerTarea();
	P(profesor)
	profesor(miTarea)
	V(profesor)	
}

process profesor(int tarea){
	gruposTermino[tarea]++
	if(gruposTermino==5){
		grupoNota=orden++;
	}
}
```
<br><br>
**6)** A una empresa llegan E empleados y por día hay T tareas para hacer (T>E), una vez que
todos los empleados llegaron empezaran a trabajar. Mientras haya tareas para hacer los
empleados tomaran una y la realizarán. Cada empleado puede tardar distinto tiempo en
realizar cada tarea. Al finalizar el día se le da un premio al empleado que más tareas
realizó. 
```
tareas[T]
canTareas=0;
numTarea=0;
tareasEmpeado[E];
sem seguir[E]=0;
sem cont=1;
prcess empleado(){
	P(cont);
	if (contador==E-1){
		V(cont);
		for(i=0;i< contador;i++){
			V(seguir[i])
		}
	}else{
		miNro=contador
		contador++;
		V(cont)
		P(seguir[miNro]);
	}
	P(semTareas)
	while(numTareas<T){
		miTarea=tareas[numTareas++]);
		V(semTareas);
		realizar(miTarea);
		P(semTE);
		tareasEmpleado(miNro)++
		V(semTE);
		P(semTareas);
	}
	V(semTareas)

}
```
