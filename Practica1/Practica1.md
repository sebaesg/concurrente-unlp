**PRACTICA 1**

<br><br>
**1)** Para el siguiente programa concurrente suponga:
<ul></ul>
<li>Que las instrucciones del siguiente código no son atómicas.</li>
<li>Todas las variables están inicializadas en 0 antes de empezar.</li>
</ul>
**Indique cual/es de las siguientes opciones son verdaderas:**
<ul>
<li>a) En algún caso el valor de x al terminar el programa es 56.:white_check_mark:</li>
<li>b) En algún caso el valor de x al terminar el programa es 22.:white_check_mark:</li>
<li>c) En algún caso el valor de x al terminar el programa es 23.:white_check_mark:</li>
<li>d) x puede obtener un valor incorrecto por interferencias.:white_check_mark:</li>
<li>e) Si las instrucciones fueran atómicas, x puede obtener un valor incorrecto por interferencias.</li>
<li>f) Si las instrucciones fueran atómicas, indique las posibles alternativas de ejecución.</li>
</ul>
<br>

|P1::|P2:: |P3::|
|--|--|--| 
| If (x = 0) {| If (x > 0) {|x:= (x*3) + (x*2) + 1;|
|y:= 4*2;|x:= x + 1;| |
|x:= y + 2;|}| |
| }| |
<br><br>
**2)** Dado un numero N verifique cuantas veces aparece ese número en un arreglo de longitud M.
Realice el algoritmo en forma concurrente utilizando <> y <await B; S>. Escriba las condiciones
que considere necesarias
<br>
```
process ejer2(n,array){
	for (i=0;i<m;i++){
		if (array[i]=n){
			<await (libre=0) encontrados++>
		}
 	}
}
```
<br><br>
**3)** En base a lo visto en la clase 2 de teoría.
<ul>
	<li>Indicar si el siguiente código funciona para resolver el problema de Productor/Consumidor
con un buffer de tamaño N. En caso de no funcionar, debe hacer las modificaciones
necesarias.</li>
	<li> Modificar el código para que funcione para C consumidores y P productores.</li>
</ul>
|int cant = 0; int pri_ocupada = 0;| int pri_vacia = 0; int buffer[N];|
|--|--|
|Productor::|Consumidor:: |
|{ while (true)|{ while (true)|
| { produce elemento|{ [await (cant > 0); cant-- ]|
| [await (cant < N); cant++]|elemento = buffer[pri_ocupada];|
| buffer[pri_vacia] = elemento;|pri_ocupada = (pri_ocupada + 1) mod N;|
| pri_vacia = (pri_vacia + 1) mod N;|consume elemento|
| }|}|
|}|}|
<ul>
	<li>En el caso de que entre un productor aumente la variable y luego se quede en la siguiente instruccion procesando , y el consumidor pasa el await dado que cant ya es mayor a 0 e intenta consumir estaria consumiendo en una posicion donde nunca se coloco el elemento por lo tanto cuando se graba en buffer y cuando se lee deberia ser anatomico </li></ul>

|int cant = 0; int pri_ocupada = 0;| int pri_vacia = 0; int buffer[N];|
|--|--|
|Productor::|Consumidor:: |
|{ while (true)|{ while (true)|
| { produce elemento|{ [await (cant > 0); cant-- ]|
| [await (cant < N); cant++;|elemento = buffer[pri_ocupada];|
| buffer[pri_vacia] = elemento];|pri_ocupada = (pri_ocupada + 1) mod N;|
| pri_vacia = (pri_vacia + 1) mod N;|consume elemento|
| }|}|
|}|}|
Modificar el código para que funcione para C consumidores y P productores.
|int cant = 0; int pri_ocupada = 0;| int pri_vacia = 0; int buffer[N];|
|--|--|
|Productor::|Consumidor:: |
|{ while (true)|{ while (true)|
| { produce elemento|{ [await (buffcon == 0); buffcon++]|
| [await (buffpro == 0); buffpro++]|[await (cant > 0); cant-- ]|
| [await (cant < N); cant++;|elemento = buffer[pri_ocupada];|
| buffer[pri_vacia] = elemento];|pri_ocupada = (pri_ocupada + 1) mod N;|
| pri_vacia = (pri_vacia + 1) mod N;|[buffcon--]|
|[buffpro--]|consume elemento|
| }|}|
|}|}|




